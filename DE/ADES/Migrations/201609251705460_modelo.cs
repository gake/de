namespace ADES.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modelo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actualizaciones",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        idPersonalMP = c.String(maxLength: 128),
                        fecha = c.DateTime(nullable: false),
                        modificaciones = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        AmpliarDenuncia_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AmpliarDenuncia", t => t.AmpliarDenuncia_id)
                .ForeignKey("dbo.PersonalMP", t => t.idPersonalMP)
                .Index(t => t.idPersonalMP)
                .Index(t => t.AmpliarDenuncia_id);
            
            CreateTable(
                "dbo.Personas",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        nombre = c.String(),
                        apellidoPaterno = c.String(),
                        apellidoMaterno = c.String(),
                        numIdentificación = c.String(),
                        curp = c.String(),
                        fechaNacimiento = c.DateTime(nullable: false),
                        telefono = c.String(),
                        celular = c.String(),
                        tipoIdentificacion = c.Int(nullable: false),
                        sexo = c.Int(nullable: false),
                        estadoCivil = c.Int(nullable: false),
                        fechaAlta = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AspNetUsers", t => t.id)
                .Index(t => t.id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Archivos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        nombreArchivo = c.String(),
                        extension = c.String(),
                        descripcion = c.String(),
                        idPersona = c.String(maxLength: 128),
                        idPersona1 = c.String(maxLength: 128),
                        idDenuncia = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        AmpliarDenuncia_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Personas", t => t.idPersona1)
                .ForeignKey("dbo.Denuncias", t => t.idDenuncia)
                .ForeignKey("dbo.AmpliarDenuncia", t => t.AmpliarDenuncia_id)
                .ForeignKey("dbo.Personas", t => t.idPersona)
                .Index(t => t.idPersona)
                .Index(t => t.idPersona1)
                .Index(t => t.idDenuncia)
                .Index(t => t.AmpliarDenuncia_id);
            
            CreateTable(
                "dbo.Direcciones",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        calle = c.String(),
                        manzana = c.String(),
                        lote = c.String(),
                        numInterior = c.Int(nullable: false),
                        numExterior = c.Int(nullable: false),
                        colonia = c.String(),
                        cp = c.String(),
                        ciudad = c.String(),
                        estado = c.String(),
                        pais = c.String(),
                        idPersona = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Personas", t => t.idPersona)
                .Index(t => t.idPersona);
            
            CreateTable(
                "dbo.AmpliarDenuncia",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        idDenuncia = c.Int(),
                        idMinisterioPublico = c.Int(),
                        fechaInicio = c.DateTime(nullable: false),
                        fechaFin = c.DateTime(nullable: false),
                        estatusEvento = c.Int(nullable: false),
                        relatoria = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        RowVersion = c.DateTime(nullable: false),
                        PersonalMP_id = c.String(maxLength: 128),
                        Denunciante_id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Denuncias", t => t.idDenuncia)
                .ForeignKey("dbo.MinisteriosPublicos", t => t.idMinisterioPublico)
                .ForeignKey("dbo.PersonalMP", t => t.PersonalMP_id)
                .ForeignKey("dbo.Denunciantes", t => t.Denunciante_id)
                .Index(t => t.idDenuncia)
                .Index(t => t.idMinisterioPublico)
                .Index(t => t.PersonalMP_id)
                .Index(t => t.Denunciante_id);
            
            CreateTable(
                "dbo.Denuncias",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        numExpediente = c.String(),
                        idDenunciante = c.String(maxLength: 128),
                        idTipoDenuncia = c.Int(),
                        folioC4 = c.String(),
                        relatoria = c.String(),
                        fecha = c.DateTime(nullable: false),
                        certificado = c.String(),
                        ipAddress = c.String(),
                        longitud = c.String(),
                        latitud = c.String(),
                        aceptacionPoliticas = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Denunciantes", t => t.idDenunciante)
                .ForeignKey("dbo.TipoDenuncias", t => t.idTipoDenuncia)
                .Index(t => t.idDenunciante)
                .Index(t => t.idTipoDenuncia);
            
            CreateTable(
                "dbo.Responsables",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nombre = c.String(),
                        telefono = c.String(),
                        edad = c.String(),
                        sexo = c.Int(nullable: false),
                        estadoCivil = c.Int(nullable: false),
                        rasgosFisicos = c.String(),
                        direccion = c.String(),
                        idDenuncia = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Denuncias", t => t.idDenuncia)
                .Index(t => t.idDenuncia);
            
            CreateTable(
                "dbo.TipoDenuncias",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nombre = c.String(),
                        descipcion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        RowVersion = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.MinisteriosPublicos",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nombre = c.String(),
                        nombreCorto = c.String(),
                        descripcion = c.String(),
                        horarios = c.String(),
                        longitud = c.String(),
                        latitud = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Denunciantes",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        huella = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Personas", t => t.id)
                .Index(t => t.id);
            
            CreateTable(
                "dbo.PersonalMP",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        numEmplead = c.Int(nullable: false),
                        mesa = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Personas", t => t.id)
                .Index(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PersonalMP", "id", "dbo.Personas");
            DropForeignKey("dbo.Denunciantes", "id", "dbo.Personas");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Actualizaciones", "idPersonalMP", "dbo.PersonalMP");
            DropForeignKey("dbo.Archivos", "idPersona", "dbo.Personas");
            DropForeignKey("dbo.AmpliarDenuncia", "Denunciante_id", "dbo.Denunciantes");
            DropForeignKey("dbo.AmpliarDenuncia", "PersonalMP_id", "dbo.PersonalMP");
            DropForeignKey("dbo.AmpliarDenuncia", "idMinisterioPublico", "dbo.MinisteriosPublicos");
            DropForeignKey("dbo.AmpliarDenuncia", "idDenuncia", "dbo.Denuncias");
            DropForeignKey("dbo.Archivos", "AmpliarDenuncia_id", "dbo.AmpliarDenuncia");
            DropForeignKey("dbo.Archivos", "idDenuncia", "dbo.Denuncias");
            DropForeignKey("dbo.Denuncias", "idTipoDenuncia", "dbo.TipoDenuncias");
            DropForeignKey("dbo.Responsables", "idDenuncia", "dbo.Denuncias");
            DropForeignKey("dbo.Denuncias", "idDenunciante", "dbo.Denunciantes");
            DropForeignKey("dbo.Actualizaciones", "AmpliarDenuncia_id", "dbo.AmpliarDenuncia");
            DropForeignKey("dbo.Direcciones", "idPersona", "dbo.Personas");
            DropForeignKey("dbo.Archivos", "idPersona1", "dbo.Personas");
            DropForeignKey("dbo.Personas", "id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.PersonalMP", new[] { "id" });
            DropIndex("dbo.Denunciantes", new[] { "id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Responsables", new[] { "idDenuncia" });
            DropIndex("dbo.Denuncias", new[] { "idTipoDenuncia" });
            DropIndex("dbo.Denuncias", new[] { "idDenunciante" });
            DropIndex("dbo.AmpliarDenuncia", new[] { "Denunciante_id" });
            DropIndex("dbo.AmpliarDenuncia", new[] { "PersonalMP_id" });
            DropIndex("dbo.AmpliarDenuncia", new[] { "idMinisterioPublico" });
            DropIndex("dbo.AmpliarDenuncia", new[] { "idDenuncia" });
            DropIndex("dbo.Direcciones", new[] { "idPersona" });
            DropIndex("dbo.Archivos", new[] { "AmpliarDenuncia_id" });
            DropIndex("dbo.Archivos", new[] { "idDenuncia" });
            DropIndex("dbo.Archivos", new[] { "idPersona1" });
            DropIndex("dbo.Archivos", new[] { "idPersona" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Personas", new[] { "id" });
            DropIndex("dbo.Actualizaciones", new[] { "AmpliarDenuncia_id" });
            DropIndex("dbo.Actualizaciones", new[] { "idPersonalMP" });
            DropTable("dbo.PersonalMP");
            DropTable("dbo.Denunciantes");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.MinisteriosPublicos");
            DropTable("dbo.TipoDenuncias");
            DropTable("dbo.Responsables");
            DropTable("dbo.Denuncias");
            DropTable("dbo.AmpliarDenuncia");
            DropTable("dbo.Direcciones");
            DropTable("dbo.Archivos");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Personas");
            DropTable("dbo.Actualizaciones");
        }
    }
}
