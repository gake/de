namespace ADES.Migrations
{
    using IdentitySample.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<IdentitySample.Models.ApplicationDbContext>
    {

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(IdentitySample.Models.ApplicationDbContext context)
        {
            bool success = false;
            var idManager = new ApplicationDbContext.IdentityManager();
            success = idManager.CreateRole("Admin");
            success = idManager.CreateRole("Denunciante");
            success = idManager.CreateRole("MP");




            ApplicationUser Administrador = new ApplicationUser()
            {
                UserName = "admin@admin.com",
                Email = "admin@admin.com",
                EmailConfirmed = true
            };

            success = idManager.CreateUser(Administrador, "Password1.");

            success = idManager.AddUserToRole(Administrador.Id, "Admin");
        }
    }
}
