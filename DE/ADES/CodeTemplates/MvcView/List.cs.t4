﻿<#@ template language="C#" HostSpecific="True" #>
<#@ output extension=".cshtml" #>
<#@ include file="Imports.include.t4" #>
@model IEnumerable<#= "<" + ViewDataTypeName + ">" #>
<#
// The following chained if-statement outputs the file header code and markup for a partial view, a view using a layout page, or a regular view.
if(IsPartialView) {
#>

<#
} else if(IsLayoutPageSelected) {
#>

@{
    ViewBag.Title = "<#= ViewName#>";
<#
if (!String.IsNullOrEmpty(LayoutPageFile)) {
#>
    Layout = "<#= LayoutPageFile#>";
<#
}
#>
}

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2><#= ViewName#></h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong><#= ViewDataTypeShortName #></strong>
            </li>
        </ol>
    </div>
</div>

<#
} else {
#>

@{
    Layout = null;
}

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title><#= ViewName #></title>
</head>
<body>
<#
    PushIndent("    ");
}
#>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de <#= ViewDataTypeShortName #></h5>
					<div class="ibox-tools">
						@Html.ActionLink("Nuevo", "Create", null, new { @class = "btn btn-primary btn-xs"})
						     <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

					</div>
                </div>
                <div class="ibox-content">

<table class="table table-striped table-bordered table-hover dataTables-example">
 <thead>
        <tr>
    <tr>
<#
IEnumerable<PropertyMetadata> properties = ModelMetadata.Properties;
foreach (PropertyMetadata property in properties) {
    if (property.Scaffold && !property.IsPrimaryKey && !property.IsForeignKey) {
#>
<#

        // This is a temporary work around until core scaffolding provides support for independent associations.
        if (property.IsAssociation && GetRelatedModelMetadata(property) == null) {
            continue;
        }
#>
        <th>
            @Html.DisplayNameFor(model => model.<#= GetValueExpression(property) #>)
        </th>
<#
    }
}
#>
        <th></th>
    </tr>
	</thead>
	 <tbody>
@foreach (var item in Model) {
    <tr>
<#
foreach (PropertyMetadata property in properties) {
    if (property.Scaffold && !property.IsPrimaryKey && !property.IsForeignKey) {
#>
<#
        // This is a temporary work around until core scaffolding provides support for independent associations.
        if (property.IsAssociation && GetRelatedModelMetadata(property) == null) {
            continue;
        }
#>
        <td>
            @Html.DisplayFor(modelItem => <#= "item." + GetValueExpression(property) #>)
        </td>
<#
    }
}

string pkName = GetPrimaryKeyName();
if (pkName != null) {
#>
        <td>
			<a class="btn btn-default" href="@Url.Action("Details", new { id = item.id })"><i class="fa fa-info" aria-hidden="true"></i></a>
            <button class="btn btn-default" onclick="edit(@item.id)" data-toggle="modal" data-target="#create-modal" ><i class="fa fa-pencil" aria-hidden="true"></i></button>
            <a href="" data-controller-name="@ViewContext.RouteData.Values["controller"]" class="btn btn-danger delete" data-toggle="modal" data-target="#eliminar" value="Eliminar" data-id="@item.id"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        </td>
<#
} else {
#>
        <td>
			<a class="btn btn-default" href="@Url.Action("Details", new { id = item.id })"><i class="fa fa-info" aria-hidden="true"></i></a>
            <button class="btn btn-default" onclick="edit(@item.id)" data-toggle="modal" data-target="#create-modal" ><i class="fa fa-pencil" aria-hidden="true"></i></button>
            <a href="" data-controller-name="@ViewContext.RouteData.Values["controller"]" class="btn btn-danger delete" data-toggle="modal" data-target="#eliminar" value="Eliminar" data-id="@item.id"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        </td>
<#
}
#>
    </tr>
}
  </tbody>
</table>

                </div>
            </div>
        </div>
    </div>
 </div>
 <#
	if(properties.Count()<=4){
		#>
		<button type="button" id="crear" class="btn btn-primary" data-toggle="modal" data-target="#create-modal">Nuevo</button>
<#	}
 
 #>



<!-- Modal de crear-->
<div class="modal inmodal" id="create-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-user-plus modal-icon"></i>
                <h4 class="modal-title">Crear</h4>
            </div>
            <div class="modal-body create-class-cache">

            </div>
        </div>
    </div>
</div>


@Html.Hidden("item-to-delete", "", new { @id = "item-to-delete" })

<!-- Delete Modal-->
<div class="modal inmodal" id="eliminar" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-trash-o modal-icon"></i>
                <h4 class="modal-title">Eliminar</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                El registro seleccionado de eliminará permanentemente. <br /> <strong>¿Está seguro que desea eliminarlo?</strong>
            </div>
            <div class="modal-footer">
                <button type="submit" value="Eliminar" class="btn btn-danger" data-dismiss="modal" id="delete-confirmation">Eliminar</button>
            </div>
        </div>
    </div>
</div>

@section Styles {
    @Styles.Render("~/Content/plugins/dataTables/dataTablesStyles")
}

@section Scripts {
    @Scripts.Render("~/plugins/jeditable")
    @Scripts.Render("~/plugins/dataTables")

<script>
        $('#crear').on('click', function () {
            $.ajax({
                url: '/@ViewContext.RouteData.Values["controller"]/Create',
                method: "GET",
                dataType: "html",
                success: function (data) {
                    var uno = $.getScript("/Scripts/jquery.validate.unobtrusive.min.js", function (data, textStatus, jqxhr) {
                        console.log(data)
                        console.log(textStatus)
                        console.log(jqxhr)
                    })

                    data['uno'] = uno;
                    $(".edit-title").text("Crear");
                    $(".create-class-cache").html(data);
                },
                error: function (e) {
                    console.log(e.responseText)
                }

            });
        });

        function edit(id) {
            $.ajax({
                url: '/@ViewContext.RouteData.Values["controller"]/Edit/' + id,
                method: "GET",
                dataType: "html",
                success: function (data) {
                    var uno = $.getScript("/Scripts/jquery.validate.unobtrusive.min.js", function (data, textStatus, jqxhr) {
                        console.log(data)
                        console.log(textStatus)
                        console.log(jqxhr)
                    })

                    data['uno'] = uno;
                    $(".edit-title").text("Editar");
                    $(".create-class-cache").html(data);
                },
                error: function (e) {
                    console.log(e.responseText)
                }

            });
        }

        $('input[type="submit"]').on('click', function () {
            $.ajax({
                url: $('form').attr('action'),
                method: "POST",
                dataType: "html",
                success: function (data) {
                    $(".create-class-cache").html(data);
                },
                error: function (e) {
                    console.log(e.responseText)
                }

            });
        });
</script>

}
<#
// The following code closes the tag used in the case of a view using a layout page and the body and html tags in the case of a regular view page
#>
<#
if(!IsPartialView && !IsLayoutPageSelected) {
    ClearIndent();
#>

</body>
</html>
<#
}
#>
<#@ include file="ModelMetadataFunctions.cs.include.t4" #>