﻿var app = angular.module("app", ["xeditable", 'datatables', 'datatables.buttons']);

var appF = angular.module("appFicha", ["xeditable", 'datatables', 'datatables.buttons']);


app.run(function (editableOptions) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});

app.controller('AutoresController', function ($scope, $http, DTOptionsBuilder) {
    $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withDOM('<"html5buttons"B>lTfgitp')
    .withButtons([
        {extend: 'copy'},
        {extend: 'csv'},
        {extend: 'excel', title: 'ExampleFile'},
        {extend: 'pdf', title: 'ExampleFile'},

        {extend: 'print',
            customize: function (win){
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');

                $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
            }
        }
    ]);

        $http.get("api/AutoresJSON").then(function (response) {
            $scope.autores = response.data;
        });

        $scope.addAutor = function () {
            $scope.inserted = {
                parametro: '',
                valor: ''
            };
            $scope.autores.push($scope.inserted);
        };
        $scope.saveAutor = function (data, id) {
            data.Rowversion = "2016-08-08";
            if (id) {
                data.id = id;
             
                $http.put('api/AutoresJSON/' + id, data).success(function (data) {
                });
            }
            else {
                $http.post('api/AutoresJSON', data).success(function (data) {
                    console.log(data);
                });
            }
        };
        $scope.removeAutor = function (index, id) {
            $scope.autores.splice(index, 1);
            $http.delete('api/AutoresJSON/' + id);
        };

});

app.controller('alumnosController', function ($scope, $http) {
    $http.get("http://api.randomuser.me/?nat=es&results=100").then(function (response) {
        $scope.alumnos = response.data.results;
    });
});

app.controller('profesoresController', function ($scope, $http) {
    $http.get("http://api.randomuser.me/?nat=es&results=100").then(function (response) {
        $scope.alumnos = response.data.results;
    });
});

appF.controller('fichaController', function ($scope, $http) {
   
});

app.controller('dirAlumnoController', function ($scope, $http) {
    $scope.DirAlum = {
        calle: '',
        manzana: '',
        lote: '',
        noInterior: '',
        noExterior: '',
        colonia: '',
        cp: '',
        ciudad: '',
        estado: '',
        pais: '',
        RowVersion: '',
        AppUser: 'Usuario Aspirante'
    };

    $scope.getAddress = function myfunction(cp) {
        alert("get Address");
        $http.get("https://api-codigos-postales.herokuapp.com/v2/codigo_postal/" + cp + "").then(function (response) {
            $scope.data = response.data;
        });
    }

    $scope.clear = function () {
        $scope.DirAlum.calle = '';
        $scope.DirAlum.manzana = '';
        $scope.DirAlum.lote = '';
        $scope.DirAlum.noInterior = '';
        $scope.DirAlum.noExterior = '';
        $scope.DirAlum.colonia = '';
        $scope.DirAlum.cp = '';
        $scope.DirAlum.ciudad = '';
        $scope.DirAlum.estado = '';
        $scope.DirAlum.pais = '';
        $scope.DirAlum.RowVersion = '';
        $scope.DirAlum.AppUser = '';
    }


    //Nuevo Registro
    $scope.save = function () {
        $scope.DirAlum.RowVersion = '2016-06-06';
        if ($scope.DirAlum != null) {
            console.log($scope.DirAlum)
            $http({
                method: 'POST',
                url: 'http://localhost:56285/api/direccionalumnos',
                data: $scope.DirAlum
            }).then(function successCallback(response) {
                $scope.clear();
                alert("Dirección Agregada !!!");

            }, function errorCallback(response) {
                alert("Error : " + response.data.ExceptionMessage);
            });
        }
        else {
            alert('Ingrese todos los campos !!');
        }
    }

    $scope.hola = function () {
        
        console.log($scope.dirAlum);
    }
});
