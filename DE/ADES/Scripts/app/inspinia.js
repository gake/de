/*
 *
 *   INSPINIA - Responsive Admin Theme
 *   version 2.4
 *
 */


$(document).ready(function () {


    // Add body-small class if window less than 768px
    if ($(this).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }

    // MetsiMenu
    $('#side-menu').metisMenu();

    // Collapse ibox function
    $('.collapse-link').click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Close ibox function
    $('.close-link').click(function () {
        var content = $(this).closest('div.ibox');
        content.remove();
    });

    // Fullscreen ibox function
    $('.fullscreen-link').click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        $('body').toggleClass('fullscreen-ibox-mode');
        button.toggleClass('fa-expand').toggleClass('fa-compress');
        ibox.toggleClass('fullscreen');
        setTimeout(function () {
            $(window).trigger('resize');
        }, 100);
    });

    // Close menu in canvas mode
    $('.close-canvas-menu').click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    });

    // Run menu of canvas
    $('body.canvas-menu .sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0.9
    });

    // Open close right sidebar
    $('.right-sidebar-toggle').click(function () {
        $('#right-sidebar').toggleClass('sidebar-open');
    });

    // Initialize slimscroll for right sidebar
    $('.sidebar-container').slimScroll({
        height: '100%',
        railOpacity: 0.4,
        wheelStep: 10
    });


    // Open close small chat
    $('.open-small-chat').click(function () {
        $(this).children().toggleClass('fa-comments').toggleClass('fa-remove');
        $('.small-chat-box').toggleClass('active');
    });

    // Initialize slimscroll for small chat
    $('.small-chat-box .content').slimScroll({
        height: '234px',
        railOpacity: 0.4
    });

    // Small todo handler
    $('.check-link').click(function () {
        var button = $(this).find('i');
        var label = $(this).next('span');
        button.toggleClass('fa-check-square').toggleClass('fa-square-o');
        label.toggleClass('todo-completed');
        return false;
    });

    // Minimalize menu
    $('.navbar-minimalize').click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();

    });

    // Tooltips demo
    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });

    // Move modal to body
    // Fix Bootstrap backdrop issu with animation.css
    $('.modal').appendTo("body");

    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

        var navbarHeigh = $('nav.navbar-default').height();
        var wrapperHeigh = $('#page-wrapper').height();

        if (navbarHeigh > wrapperHeigh) {
            $('#page-wrapper').css("min-height", navbarHeigh + "px");
        }

        if (navbarHeigh < wrapperHeigh) {
            $('#page-wrapper').css("min-height", $(window).height() + "px");
        }

        if ($('body').hasClass('fixed-nav')) {
            $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
        }

    }

    fix_height();

    // Fixed Sidebar
    $(window).bind("load", function () {
        if ($("body").hasClass('fixed-sidebar')) {
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            });
        }
    });

    // Move right sidebar top after scroll
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
            $('#right-sidebar').addClass('sidebar-top');
        } else {
            $('#right-sidebar').removeClass('sidebar-top');
        }
    });

    $(window).bind("load resize scroll", function () {
        if (!$("body").hasClass('body-small')) {
            fix_height();
        }
    });

    $("[data-toggle=popover]")
        .popover();


    // Add slimscroll to element
    $('.full-height-scroll').slimscroll({
        height: '100%'
    })
});


// Minimalize menu when screen is less than 768px
$(window).bind("resize", function () {
    if ($(this).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }
});

// Local Storage functions
// Set proper body class and plugins based on user configuration
$(document).ready(function () {
    if (localStorageSupport) {

        var collapse = localStorage.getItem("collapse_menu");
        var fixedsidebar = localStorage.getItem("fixedsidebar");
        var fixednavbar = localStorage.getItem("fixednavbar");
        var boxedlayout = localStorage.getItem("boxedlayout");
        var fixedfooter = localStorage.getItem("fixedfooter");

        var body = $('body');

        if (fixedsidebar == 'on') {
            body.addClass('fixed-sidebar');
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            });
        }

        if (collapse == 'on') {
            if (body.hasClass('fixed-sidebar')) {
                if (!body.hasClass('body-small')) {
                    body.addClass('mini-navbar');
                }
            } else {
                if (!body.hasClass('body-small')) {
                    body.addClass('mini-navbar');
                }

            }
        }

        if (fixednavbar == 'on') {
            $(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');
            body.addClass('fixed-nav');
        }

        if (boxedlayout == 'on') {
            body.addClass('boxed-layout');
        }

        if (fixedfooter == 'on') {
            $(".footer").addClass('fixed');
        }
    }
});

// check if browser support HTML5 local storage
function localStorageSupport() {
    return (('localStorage' in window) && window['localStorage'] !== null)
}

// For demo purpose - animation css script
function animationHover(element, animation) {
    element = $(element);
    element.hover(
        function () {
            element.addClass('animated ' + animation);
        },
        function () {
            //wait for animation to finish before removing classes
            window.setTimeout(function () {
                element.removeClass('animated ' + animation);
            }, 2000);
        });
}

function SmoothlyMenu() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400);
            }, 200);
    } else if ($('body').hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400);
            }, 100);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
}

// Dragable panels
function WinMove() {
    var element = "[class*=col]";
    var handle = ".ibox-title";
    var connect = "[class*=col]";
    $(element).sortable(
        {
            handle: handle,
            connectWith: connect,
            tolerance: 'pointer',
            forcePlaceholderSize: true,
            opacity: 0.8
        })
        .disableSelection();
}


// DataTable init

$(document).ready(function () {

    var table = $('.dataTables-example').DataTable({
        responsive: true,
        //"columns": [
        //{ "width": "100px" },
        // { "width": "20%" },
        //   { "width": "30%" },

        //               { "width": "30%" },

        //     null,
        //      null,
        //       null,
        //        null,
        //         null
        //],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "Sin registros.",
            "info": "Pagina _PAGE_ de _PAGES_",
            "search": "",
            "infoEmpty": "Sin resultados.",
            "infoFiltered": "(filtered from _MAX_ total records)",
            searchPlaceholder: "Buscar registros",
            fixedColumns: {
                leftColumns: 2
            },
            paginate: {
                first: 'Primera p�gina',
                last: '�ltima p�gina',
                previous: 'Anterior',
                next: 'Siguiente'
            },
        },
        //"aoColumns": [{ "bVisible": true }],
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy' },
            { extend: 'csv' },
            { extend: 'excel', title: 'ExampleFile' },
            { extend: 'pdf', title: 'ExampleFile' },

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]



    });
});
var MaxInputs = 12; //N�mero Maximo de Campos
//var x = n�mero de campos existentes en el contenedor
var x;
var FieldCount; //para el seguimiento de los campos
var contenedor = $("#contenedor"); //ID del contenedor

function agregarboton(parametro, contenedor,placeholder) {
    x = $("#" + contenedor + " div").length + 1;
    FieldCount = x - 1; //para el seguimiento de los campos
    if (x <= MaxInputs) //max input box allowed
    {
        contenedor = $("#" + contenedor);
        console.log(x);
        FieldCount++;
        //agregar campo
        $(contenedor).append('<div class="col-lg-4"><input type="text" class="form-control" name="' + parametro + '" id="campo_' + FieldCount + '" placeholder="'+placeholder+' ' + FieldCount + '" /><a href="#" class="eliminar">&times;</a></div>');
        x++; //text box increment
    }
    return false;
}
var MaxInputsRed = 28; //N�mero Maximo de Campos
//var x = n�mero de campos existentes en el contenedor
var xRed;
var FieldCountRed; //para el seguimiento de los campos
var contenedorRed = $("#contenedorRed"); //ID del contenedor
placeholderFieldCountRed = 1;
function agregarbotonRedsocial(nameSelect, contenedorRed, placeholder,nameInput) {
    xRed = $("#" + contenedorRed + " div").length + 1;
    FieldCountRed = xRed - 1; //para el seguimiento de los campos
    if (xRed <= MaxInputsRed) //max input box allowed
    {
        contenedorRed = $("#" + contenedorRed);
        console.log('nuevo');
        FieldCountRed++;
        placeholderFieldCountRed++;
        nemeCount = placeholderFieldCountRed - 1;

        //agregar campo
        $(contenedorRed).append('<div class="col-lg-4"><div class="form-group"><select class="form-control valid" name="' + '[' + nemeCount + '].socialMedia"><option value="0">P�gina WEB</option><option value="1">Facebook</option><option value="2">Twitter</option><option value="3">Youtube</option><option value="4">Instagram</option><option value="5">Vimeo</option><option value="6">Google Plus</option><option value="7">Linkedin</option><option value="8">Skype</option><option value="9">Blogger</option><option value="10">Pinterest</option><option value="11">Otro</option></select></div> </div><div class="col-lg-8"><div class="form-group"><input type="text" class="form-control" name="[' + nemeCount + '].url" id="campo_1" placeholder="' + placeholder + ' ' + placeholderFieldCountRed + '" /></div></div>');
        xRed++; //text box increment
    }
    return false;
}
var MaxInputsRed = 28; //N�mero Maximo de Campos
//var x = n�mero de campos existentes en el contenedor
var xRed;
var FieldCountRed; //para el seguimiento de los campos
var nameCountBecas = 0;//numero para el array de nombre de Recados
function agregarbotonBecas(nameSelect, contenedorRed, placeholder, nameInput) {
    xRed = $("#" + contenedorRed + " div").length + 1;
    FieldCountRed = xRed - 1; //para el seguimiento de los campos
    if (xRed <= MaxInputsRed) //max input box allowed
    {
        contenedorRed = $("#" + contenedorRed);
        FieldCountRed++;
        nameCountBecas++;
        //agregar campo
        $(contenedorRed).append('<div class="row"><div class="col-lg-4"><div class="form-group"><label>Nombre(s)</label> <input id="[' + nameCountBecas + '].nombre" name="[' + nameCountBecas + '].nombre" type="text" class="form-control"></div></div> <div class="col-lg-4"><div class="form-group"><label>Monto</label><input id="[' + nameCountBecas + '].monto" name="[' + nameCountBecas + '].monto" type="number" class="form-control"></div> </div></div>');
        xRed++; //text box increment
    }
    return false;
}
//var x = n�mero de campos existentes en el contenedor
var xRed;
var FieldCountRed; //para el seguimiento de los campos
var nameCountRecados = 0;//numero para el array de nombre de Recados
function agregarbotonRecados(nameSelect, contenedorRed, placeholder, nameInput) {
    xRed = $("#" + contenedorRed + " div").length + 1;
    FieldCountRed = xRed - 1; //para el seguimiento de los campos
    if (xRed <= MaxInputsRed) //max input box allowed
    {
        contenedorRed = $("#" + contenedorRed);
        FieldCountRed++;
        nameCountRecados++;
        //agregar campo
        $(contenedorRed).append('<div class="row" style=" border-top:2px solid #ccc; padding-top:15px;"><div class="col-lg-4"><div class="form-group"><label>Nombre(s) del tutor</label><input id="[' + nameCountRecados + '].nombre" name="[' + nameCountRecados + '].nombre" type="text" class="form-control"></div></div><div class="col-lg-4"><div class="form-group"><label>Apellido Paterno</label><input id="[' + nameCountRecados + '].apellidoPaterno" name="[' + nameCountRecados + '].apellidoPaterno" type="text" class="form-control"></div></div><div class="col-lg-4"><div class="form-group"><label>Apellido Materno</label><input id="[' + nameCountRecados + '].apellidoMaterno" name="[' + nameCountRecados + '].apellidoMaterno" type="text" class="form-control col-lg-1"></div></div><div class="col-lg-6"><div class="form-group"><label>Tel�fono</label><input id="[' + nameCountRecados + '].telefono" name="[' + nameCountRecados + '].telefono" type="number" class="form-control"></div></div></div>');
        xRed++; //text box increment
    }
    return false;
}
var MaxInputsRed = 28; //N�mero Maximo de Campos
//var x = n�mero de campos existentes en el contenedor
var xRed;
var FieldCountRed; //para el seguimiento de los campos
var nameCountTutor = 0;//numero para el array de nombre de TUTOR
var contenedorRed = $("#contenedorRed"); //ID del contenedor
placeholderFieldCountRed = 1;
function agregarbotonTutor(nameSelect, contenedorRed, placeholder, nameInput) {
    xRed = $("#" + contenedorRed + " div").length + 1;
    FieldCountRed = xRed - 1; //para el seguimiento de los campos
    if (xRed <= MaxInputsRed) //max input box allowed
    {
        contenedorRed = $("#" + contenedorRed);
        console.log('nuevo');
        FieldCountRed++;
        nameCountTutor++;
        //agregar campo
        $(contenedorRed).append('<div class="row" style=" border-top:2px solid #ccc; padding-top:15px;"><div class="col-lg-4"><div class="form-group"><label>Nombre(s) del tutor</label><input id="tutores[' + nameCountTutor + ']_nombre" name="tutores[' + nameCountTutor + '].nombre" type="text" class="form-control"></div></div><div class="col-lg-4"><div class="form-group"><label>Apellido Paterno</label><input id="tutores[' + nameCountTutor + ']_apellidoPaterno" name="tutores[' + nameCountTutor + '].apellidoPaterno" type="text" class="form-control"></div></div><div class="col-lg-4"><div class="form-group"><label>Apellido Materno</label><input id="tutores[' + nameCountTutor + ']_apellidoMaterno" name="tutores[' + nameCountTutor + '].apellidoMaterno" type="text" class="form-control col-lg-1"></div></div><div class="col-lg-6"><div class="form-group"><label>Tel�fono</label><input id="tutores[' + nameCountTutor + ']_telefono" name="tutores[' + nameCountTutor + '].telefono" class="form-control"></div></div><div class="col-lg-6"><div class="form-group"><label class="col-lg-12">Direcci�n</label><div class="col-lg-8"><input id="tutores[' + nameCountTutor + ']_direccion" name="tutores[' + nameCountTutor + '].direccion" type="text" class="form-control "></div></div></div></div>');
        xRed++; //text box increment
    }
    return false;
}
var MaxInputsIdioma = 28; //N�mero Maximo de Campos
var nameCountIdioma = 0;//numero para el array de nombre de idioma
var modelname = 'Model';
function agregarbotonIdioma(parametro, contenedor, placeholder) {
    x = $("#" + contenedor + " div").length + 1;
    FieldCount = x - 1; //para el seguimiento de los campos
    if (x <= MaxInputsIdioma) //max input box allowed
    {
        contenedor = $("#" + contenedor);
        console.log(x);
        FieldCount++;
        nameCountIdioma++;
        //agregar campo
        $(contenedor).append('<div class="col-lg-6"><div class="form-group"><label>Idioma</label><input name="' + modelname + '[' + nameCountIdioma + '].idioma" type="text" class="form-control" placeholder="Idioma"></div></div><div class="col-lg-2"><div class="form-group"><label>L</label><input name="' + modelname + '[' + nameCountIdioma + '].lectura" type="text" class="form-control" placeholder="Lectura"></div></div><div class="col-lg-2"><div class="form-group"><label>E</label><input name="' + modelname + '[' + nameCountIdioma + '].escritura" type="text" class="form-control" placeholder="Escritura"></div></div><div class="col-lg-2"><div class="form-group"><label>C</label><input name="' + modelname + '[' + nameCountIdioma + '].comprension" type="text" class="form-control" placeholder="Comprensi�n"> </div></div>');
        x++; //text box increment
    }
    return false;
}
var MaxInputsDialecto = 28; //N�mero Maximo de Campos
var nameCountDialecto = 0; //numero para el array de nombre de dialecto
var modelname1="Model1"
function agregarbotonDialecto(parametro, contenedor, placeholder) {
    x = $("#" + contenedor + " div").length + 1;
    FieldCount = x - 1; //para el seguimiento de los campos
    if (x <= MaxInputsDialecto) //max input box allowed
    {
        contenedor = $("#" + contenedor);
        console.log(x);
        FieldCount++;
        nameCountDialecto++;
        //agregar campo
        $(contenedor).append('<div class="col-lg-6"><div class="form-group"><label>Dialecto</label><input name="' + modelname1 + '[' + nameCountDialecto + '].dialecto" type="text" class="form-control" placeholder="Dialecto"></div></div><div class="col-lg-2"><div class="form-group"><label>L</label><input name="' + modelname1 + '[' + nameCountDialecto + '].lectura" type="text" class="form-control" placeholder="Lectura"></div></div><div class="col-lg-2"><div class="form-group"><label>E</label><input name="' + modelname1 + '[' + nameCountDialecto + '].escritura" type="text" class="form-control" placeholder="Escritura"></div></div><div class="col-lg-2"><div class="form-group"><label>C</label><input name="' + modelname1 + '[' + nameCountDialecto + '].comprension" type="text" class="form-control" placeholder="Comprensi�n"> </div></div>'); x++; //text box increment
    }
    return false;
}

function agregarbotonDiscapacidad(parametro, contenedor, placeholder) {
    x = $("#" + contenedor + " div").length + 1;
    FieldCount = x - 1; //para el seguimiento de los campos
    if (x <= MaxInputs) //max input box allowed
    {
        contenedor = $("#" + contenedor);
        console.log(x);
        FieldCount++;
        //agregar campo
        $(contenedor).append('<div class="col-lg-8"><input type="text" class="form-control" name="' + parametro + '" id="campo_' + FieldCount + '" placeholder="' + placeholder + ' ' + FieldCount + '" /><a href="#" class="eliminar">&times;</a></div>');
        x++; //text box increment
    }
    return false;
}
$(document).ready(function () {
    $("body").on("click", ".eliminar", function (e) { //click en eliminar campo

        if (x > 1) {
            $(this).parent('div').remove(); //eliminar el campo
            x--;
        }
        return false;
    });
});;


// Eliminar
// Para que funcione este script en cualquier crud simplemente agregar esto en el boton eliminar
//data-controller-name="@ViewContext.RouteData.Values["controller"]"

$(document).ready(function () {
    $(".dataTables-example tbody").on("click", '.delete', function (e) {
        e.preventDefault();

        // Asigan el id del elemento al input hidden
        var id = $(this).data('id');
        var controllerName = $(this).data('controller-name');
        var hidden = $('#item-to-delete');
        hidden.val(id);
        hidden.attr('data-controller', controllerName)
    });

    //Submit del modal
    $('#delete-confirmation').on('click', function () {

        // Obtiene el id del input hidden
        var id = $('#item-to-delete').val();
        var token = $(':input:hidden[name*="__RequestVerificationToken"]').val();
        var data = {};
        data['__RequestVerificationToken'] = token
        var url = $('#item-to-delete').attr('data-controller') + '/Delete/' + id

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function (result) {
                location.reload();
            },
            error: function (e) {
                console.log(e.responseText)
            }
        });

        return false;
    });
});
