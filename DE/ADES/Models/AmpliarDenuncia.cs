﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    [Table("dbo.AmpliarDenuncia")]
    public class AmpliarDenuncia
    {
        public int id { get; set; }
        [ForeignKey("Denuncia"), Display(Name = "Denuncia")]
        public int? idDenuncia { get; set; }
        public virtual Denuncia Denuncia { get; set; }
        [ForeignKey("MinisterioPublico"), Display(Name = "Ministerio Publico")]
        public int? idMinisterioPublico { get; set; }
        public virtual MinisterioPublico MinisterioPublico{ get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.DateTime)]
        public DateTime fechaInicio { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.DateTime)]
        public DateTime fechaFin { get; set; }
        public EstatusEvento estatusEvento { get; set; }
        public string relatoria { get; set; }
        [Display(Name = "Personal MP")]        
        public virtual PersonalMP PersonalMP { get; set; }
        public virtual ICollection<ArchivoEvidencia> ArchivosEvidencia{ get; set; }
        public virtual ICollection<Actualizacion> Actualizaciones { get; set; }
        public bool IsDeleted { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime RowVersion { get; set; }

    }

    public enum EstatusEvento { 
        programado,
        Proceso,
        finalizado,
        cancelado,
        otro
    }
}