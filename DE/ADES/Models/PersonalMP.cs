﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    [Table("dbo.PersonalMP")]
    public class PersonalMP : Persona
    {
        [Display(Name = "Número de empleado")]
        public int numEmplead { get; set; }
        [Display(Name = "Mesa")]
        public string mesa { get; set; }

    }
}