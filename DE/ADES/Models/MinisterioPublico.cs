﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    [Table("dbo.MinisteriosPublicos")]
    public class MinisterioPublico
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string nombreCorto { get; set; }
        public string descripcion { get; set; }
        public string horarios { get; set; }
        public string longitud { get; set; }
        public string latitud { get; set; }
    }
}