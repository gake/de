﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    [Table("dbo.Denunciantes")]
    public class Denunciante : Persona    {
        public string huella { get; set; }
        public virtual ICollection<Denuncia> Denuncias{ get; set; }
        public virtual ICollection<AmpliarDenuncia> AmpliacionesDenuncias { get; set; }

    }
}