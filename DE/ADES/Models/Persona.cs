﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using IdentitySample.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ADES.Models
{
    [Table("dbo.Personas")]
    public abstract class Persona
    {
        [Key, ForeignKey("ApplicationUser")]
        public string id { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public string nombre { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }
        public string numIdentificación { get; set; }
        public string curp { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime fechaNacimiento { get; set; }
        public string telefono { get; set; }
        public string celular { get; set; }
        public TipoIdentificacion tipoIdentificacion { get; set; }
        public Sexo sexo { get; set; }
        public EstadoCivil estadoCivil { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime fechaAlta { get; set; }
        public virtual ICollection<Direccion> Direcciones { get; set; }
        public virtual ICollection<ArchivoFoto> ArchivosFotos { get; set; }
        public virtual ICollection<ArchivoIdentificacion> ArchivosIdentificacion { get; set; }




    }

    public enum TipoIdentificacion
    {
        [Display(Name="IFE")]
        ife,
        [Display(Name="CÉDULA")]
        cedula,
        [Display(Name = "PASAPORTE")]
        pasaporte,
        [Display(Name = "LICENCIA")]
        licencia
    }

    public enum EstadoCivil {
        [Display(Name = "SOLTERO/A")]
        soltero,
        [Display(Name = "CASADO/A")]
        casado,
        [Display(Name = "UNIÓN LIBRE")]
        unionlibre,
        [Display(Name = "DIVORCIADO/A")]
        divorciado,
        [Display(Name = "VIUDO")]
        viudo,
        [Display(Name = "OTRO")]
        otro    
    }

    public enum Sexo {
        [Display(Name = "MASCULINO")]
        masculino,
        [Display(Name = "FEMENINO")]
        femenino
    }
}