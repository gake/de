﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    [Table("dbo.Archivos")]
    public class Archivo
    {
        [Key]
        public Guid Id { get; set; }
        public string nombreArchivo { get; set; }
        public string extension { get; set; }
        public string descripcion { get; set; }
    }
}