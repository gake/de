﻿using ADES.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentitySample.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public virtual DbSet<Actualizacion> Actualizaciones { get; set; }
        public virtual DbSet<AmpliarDenuncia> AmpliacionesDenuncias { get; set; }
        public virtual DbSet<Archivo> Archivos { get; set; }
        public virtual DbSet<Denuncia> Denuncias { get; set; }
        public virtual DbSet<Denunciante> Denunciantes { get; set; }
        public virtual DbSet<Direccion> Direcciones { get; set; }
        public virtual DbSet<MinisterioPublico> MinisteriosPublicos { get; set; }
        public virtual DbSet<Persona> Persona { get; set; }
        public virtual DbSet<PersonalMP> PersonalesMP { get; set; }
        public virtual DbSet<Responsable> Responsable { get; set; }
        public virtual DbSet<TipoDenuncia> TiposDenuncias { get; set; }



        static ApplicationDbContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public class IdentityManager
        {
            public bool RoleExists(string name)
            {
                var rm = new RoleManager<IdentityRole>(
                    new RoleStore<IdentityRole>(new ApplicationDbContext()));
                return rm.RoleExists(name);
            }


            public bool CreateRole(string name)
            {
                var rm = new RoleManager<IdentityRole>(
                    new RoleStore<IdentityRole>(new ApplicationDbContext()));
                var idResult = rm.Create(new IdentityRole(name));
                return idResult.Succeeded;
            }


            public bool CreateUser(ApplicationUser user, string password)
            {
                var um = new UserManager<ApplicationUser>(
                    new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var idResult = um.Create(user, password);
                return idResult.Succeeded;
            }


            public bool AddUserToRole(string userId, string roleName)
            {
                var um = new UserManager<ApplicationUser>(
                    new UserStore<ApplicationUser>(new ApplicationDbContext()));
                var idResult = um.AddToRole(userId, roleName);
                return idResult.Succeeded;
            }
        }

    }

    public class AdesConfiguration : DbConfiguration
    {
        public AdesConfiguration()
        {

        }
    }


}