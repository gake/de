﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    [Table("dbo.Direcciones")]
    public class Direccion
    {
        public int id { get; set; }
        public string calle { get; set; }
        public string manzana { get; set; }
        public string lote { get; set; }
        public int numInterior { get; set; }
        public int numExterior { get; set; }
        public string colonia { get; set; }
        public string cp { get; set; }
        public string ciudad { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
        [ForeignKey("Persona"), Display(Name = "Persona")]
        public string idPersona { get; set; }
        public virtual Persona Persona { get; set; }

    }
}