﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    public class ArchivoFoto : Archivo
    {
        [ForeignKey("Persona")]
        public string idPersona { get; set; }
        public virtual Persona Persona { get; set; }
    }
}