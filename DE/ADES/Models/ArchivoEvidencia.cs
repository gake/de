﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    public class ArchivoEvidencia : Archivo
    {
        [ForeignKey("Denuncia")]
        public int? idDenuncia { get; set; }
        public virtual Denuncia Denuncia { get; set; }

    }
}