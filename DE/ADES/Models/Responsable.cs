﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    [Table("dbo.Responsables")]
    public class Responsable
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string telefono { get; set; }
        public string edad { get; set; }
        public Sexo sexo { get; set; }
        public EstadoCivil estadoCivil { get; set; }
        public string rasgosFisicos { get; set; }
        public string direccion { get; set; }
        [ForeignKey("Denuncia")]
        public int? idDenuncia { get; set; }
        public virtual Denuncia Denuncia { get; set; }
    }
}