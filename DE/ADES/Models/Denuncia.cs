﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    [Table("dbo.Denuncias")]
    public class Denuncia 
    {
        public int id { get; set; }
        [Display(Name = "Número de Expediente")]
        public string numExpediente { get; set; }
        [ForeignKey("Denunciante")]
        public string idDenunciante { get; set; }
        public virtual Denunciante Denunciante { get; set; }
        [ForeignKey("TipoDenuncia"), Display(Name = "Tipo denuncia")]
        public int? idTipoDenuncia { get; set; }
        public virtual TipoDenuncia TipoDenuncia { get; set; }
        [Display(Name = "Folio")]
        public string folioC4 { get; set; }
        [Display(Name = "Relatoría")]
        public string relatoria { get; set; }
        [Display(Name = "Fecha")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime fecha { get; set; }
        [Display(Name = "Certificado")]
        public string certificado { get; set; }
        [Display(Name = "ip")]
        public string ipAddress { get; set; }
        [Display(Name = "Longitud")]
        public string longitud { get; set; }
        [Display(Name = "Latitud")]
        public string latitud { get; set; }
        [Display(Name = "Aceptación de politicas")]
        public bool aceptacionPoliticas { get; set; }
        public virtual ICollection<AmpliarDenuncia> AmpliacionesDenuncias{ get; set; }       
        public virtual ICollection<Responsable> Responsables { get; set; }
    }
}