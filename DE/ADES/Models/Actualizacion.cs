﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ADES.Models
{
    [Table("dbo.Actualizaciones")]
    public class Actualizacion
    {
        public int id { get; set; }
        [ForeignKey("PersonalMP"), Display(Name = "Personal MP")]
        public string idPersonalMP { get; set; }
        public virtual PersonalMP PersonalMP { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime fecha { get; set; }
        public string modificaciones { get; set; }
        public bool IsDeleted { get; set; }
       
    }
}