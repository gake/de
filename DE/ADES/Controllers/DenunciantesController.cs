﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ADES.Models;
using IdentitySample.Models;

namespace ADES.Controllers
{
    public class DenunciantesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Denunciantes/
        public ActionResult Index()
        {
            var persona = db.Persona.OfType<Denunciante>().Include(d => d.ApplicationUser);
            return View(persona.ToList());
        }

        // GET: /Denunciantes/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Denunciante denunciante = (Denunciante)db.Persona.Find(id);
            if (denunciante == null)
            {
                return HttpNotFound();
            }
            return View(denunciante);
        }

        // GET: /Denunciantes/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Users, "Id", "Email");
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }


        // POST: /Denunciantes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,nombre,apellidoPaterno,apellidoMaterno,numIdentificación,curp,fechaNacimiento,telefono,celular,tipoIdentificacion,sexo,estadoCivil,fechaAlta,huella")] Denunciante denunciante)
        {
            if (ModelState.IsValid)
            {
                db.Persona.Add(denunciante);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id = new SelectList(db.Users, "Id", "Email", denunciante.id);
			 if (Request.IsAjaxRequest())
                return PartialView(denunciante);
            else
                return View(denunciante);
        }

        // GET: /Denunciantes/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Denunciante denunciante = (Denunciante)db.Persona.Find(id);
            if (denunciante == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Users, "Id", "Email", denunciante.id);
 
			 if (Request.IsAjaxRequest())
                return PartialView(denunciante);
            else
                return View(denunciante);
        }

        // POST: /Denunciantes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,nombre,apellidoPaterno,apellidoMaterno,numIdentificación,curp,fechaNacimiento,telefono,celular,tipoIdentificacion,sexo,estadoCivil,fechaAlta,huella")] Denunciante denunciante)
        {
            if (ModelState.IsValid)
            {
                db.Entry(denunciante).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id = new SelectList(db.Users, "Id", "Email", denunciante.id);
            if (Request.IsAjaxRequest())
                return PartialView(denunciante);
            else
                return View(denunciante);
        }

        // GET: /Denunciantes/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Denunciante denunciante = (Denunciante)db.Persona.Find(id);
            if (denunciante == null)
            {
                return HttpNotFound();
            }
            return View(denunciante);
        }

        // POST: /Denunciantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Denunciante denunciante = (Denunciante)db.Persona.Find(id);
            db.Persona.Remove(denunciante);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
