﻿using CrystalDecisions.CrystalReports.Engine;
using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Web.Mvc;
using SendGrid;
using System.Net.Mail;
using System.Net;
using IdentitySample.Models;

using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Threading;
using System.Diagnostics;

namespace IdentitySample.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            /*ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Expediente/Acuse.rpt")));
            rd.SetParameterValue("folio", "123");
            rd.SetParameterValue("fecha", DateTime.Now.ToShortDateString());
            rd.SetParameterValue("hora", DateTime.Now.ToShortTimeString());
            rd.SetParameterValue("denuncia", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
            rd.SetParameterValue("certificado", 0);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);


            SendAsyncDocument("admin@utancun.edu.mx", "gake@ozelot.it", "Acuse", "Hola gerardo", stream, "Acuse.pdf");

            //return File(stream, "Application/pdf", "Reporte.pdf");*/
            return View();
        }

        public void SendAsyncDocument(string from, string to, string subject, string body, Stream document, string name)
        {
            string username = "azure_d1ab1a66f08549e4a848e9eee4d49010@azure.com";
            string pswd = "Ozelot2015";
            SendGridMessage myMessage = new SendGridMessage();
            Console.WriteLine("---------------------- Enviado -----------------------");
            myMessage.AddTo(to);
            myMessage.From = new MailAddress(from, "UT-MATEDI");
            myMessage.Subject = subject;
            myMessage.Text = body;
            //myMessage.AddAttachment(@"C:\Users\Moisés Torres\Documents\Visual Studio 2015\Projects\SmartBaby\SmartBaby\Brigada 09-Abril-2016.xlsx");
            myMessage.AddAttachment(document, name);
            var credentials = new NetworkCredential(username, pswd);

            var transportWeb = new Web(credentials);

            transportWeb.DeliverAsync(myMessage);
        }

        public ActionResult Dashboard()
        {
            string idUser = User.Identity.GetUserId();
            ApplicationUser usuario = db.Users.Find(idUser);
            ViewBag.denuncias = db.Denuncias.Where(d => d.idDenunciante == usuario.Id).ToList();
            return View();
        }

        public ActionResult Alumno()
        {
            return View();
        }

        public ActionResult Profesor()
        {
            return View();
        }

        public ActionResult Servicios()
        {
            return View();
        }

        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
