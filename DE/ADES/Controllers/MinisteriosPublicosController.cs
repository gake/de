﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ADES.Models;
using IdentitySample.Models;

namespace ADES.Controllers
{
    public class MinisteriosPublicosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /MinisteriosPublicos/
        public ActionResult Index()
        {
            return View(db.MinisteriosPublicos.ToList());
        }

        // GET: /MinisteriosPublicos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MinisterioPublico ministerioPublico = db.MinisteriosPublicos.Find(id);
            if (ministerioPublico == null)
            {
                return HttpNotFound();
            }
            return View(ministerioPublico);
        }

        // GET: /MinisteriosPublicos/Create
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }


        // POST: /MinisteriosPublicos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,nombre,nombreCorto,descripcion,horarios,longitud,latitud")] MinisterioPublico ministerioPublico)
        {
            if (ModelState.IsValid)
            {
                db.MinisteriosPublicos.Add(ministerioPublico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

			 if (Request.IsAjaxRequest())
                return PartialView(ministerioPublico);
            else
                return View(ministerioPublico);
        }

        // GET: /MinisteriosPublicos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MinisterioPublico ministerioPublico = db.MinisteriosPublicos.Find(id);
            if (ministerioPublico == null)
            {
                return HttpNotFound();
            }
 
			 if (Request.IsAjaxRequest())
                return PartialView(ministerioPublico);
            else
                return View(ministerioPublico);
        }

        // POST: /MinisteriosPublicos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,nombre,nombreCorto,descripcion,horarios,longitud,latitud")] MinisterioPublico ministerioPublico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ministerioPublico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView(ministerioPublico);
            else
                return View(ministerioPublico);
        }

        // GET: /MinisteriosPublicos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MinisterioPublico ministerioPublico = db.MinisteriosPublicos.Find(id);
            if (ministerioPublico == null)
            {
                return HttpNotFound();
            }
            return View(ministerioPublico);
        }

        // POST: /MinisteriosPublicos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MinisterioPublico ministerioPublico = db.MinisteriosPublicos.Find(id);
            db.MinisteriosPublicos.Remove(ministerioPublico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
