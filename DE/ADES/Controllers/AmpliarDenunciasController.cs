﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ADES.Models;
using IdentitySample.Models;

namespace ADES.Controllers
{
    public class AmpliarDenunciasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /AmpliarDenuncias/
        public ActionResult Index()
        {
            var ampliacionesdenuncias = db.AmpliacionesDenuncias.Include(a => a.Denuncia).Include(a => a.MinisterioPublico);
            return View(ampliacionesdenuncias.ToList());
        }

        // GET: /AmpliarDenuncias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmpliarDenuncia ampliarDenuncia = db.AmpliacionesDenuncias.Find(id);
            if (ampliarDenuncia == null)
            {
                return HttpNotFound();
            }
            return View(ampliarDenuncia);
        }

        // GET: /AmpliarDenuncias/Create
        public ActionResult Create()
        {
            ViewBag.idDenuncia = new SelectList(db.Denuncias, "id", "numExpediente");
            ViewBag.idMinisterioPublico = new SelectList(db.MinisteriosPublicos, "id", "nombre");
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }


        // POST: /AmpliarDenuncias/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,idDenuncia,idMinisterioPublico,fechaInicio,fechaFin,estatusEvento,relatoria,IsDeleted,RowVersion")] AmpliarDenuncia ampliarDenuncia)
        {
            if (ModelState.IsValid)
            {
                db.AmpliacionesDenuncias.Add(ampliarDenuncia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idDenuncia = new SelectList(db.Denuncias, "id", "numExpediente", ampliarDenuncia.idDenuncia);
            ViewBag.idMinisterioPublico = new SelectList(db.MinisteriosPublicos, "id", "nombre", ampliarDenuncia.idMinisterioPublico);
			 if (Request.IsAjaxRequest())
                return PartialView(ampliarDenuncia);
            else
                return View(ampliarDenuncia);
        }

        // GET: /AmpliarDenuncias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmpliarDenuncia ampliarDenuncia = db.AmpliacionesDenuncias.Find(id);
            if (ampliarDenuncia == null)
            {
                return HttpNotFound();
            }
            ViewBag.idDenuncia = new SelectList(db.Denuncias, "id", "numExpediente", ampliarDenuncia.idDenuncia);
            ViewBag.idMinisterioPublico = new SelectList(db.MinisteriosPublicos, "id", "nombre", ampliarDenuncia.idMinisterioPublico);
 
			 if (Request.IsAjaxRequest())
                return PartialView(ampliarDenuncia);
            else
                return View(ampliarDenuncia);
        }

        // POST: /AmpliarDenuncias/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,idDenuncia,idMinisterioPublico,fechaInicio,fechaFin,estatusEvento,relatoria,IsDeleted,RowVersion")] AmpliarDenuncia ampliarDenuncia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ampliarDenuncia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idDenuncia = new SelectList(db.Denuncias, "id", "numExpediente", ampliarDenuncia.idDenuncia);
            ViewBag.idMinisterioPublico = new SelectList(db.MinisteriosPublicos, "id", "nombre", ampliarDenuncia.idMinisterioPublico);
            if (Request.IsAjaxRequest())
                return PartialView(ampliarDenuncia);
            else
                return View(ampliarDenuncia);
        }

        // GET: /AmpliarDenuncias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmpliarDenuncia ampliarDenuncia = db.AmpliacionesDenuncias.Find(id);
            if (ampliarDenuncia == null)
            {
                return HttpNotFound();
            }
            return View(ampliarDenuncia);
        }

        // POST: /AmpliarDenuncias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AmpliarDenuncia ampliarDenuncia = db.AmpliacionesDenuncias.Find(id);
            db.AmpliacionesDenuncias.Remove(ampliarDenuncia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
