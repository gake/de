﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ADES.Models;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using SendGrid;
using System.Net.Mail;

namespace ADES.Controllers
{
    public class DenunciasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Denuncias/
        public ActionResult Index()
        {
            var denuncias = db.Denuncias.Include(d => d.Denunciante).Include(d => d.TipoDenuncia);
            return View(denuncias.ToList());
        }

        public void SendAsyncDocument(string from, string to, string subject, string body, Stream document, string name)
        {
            string username = "azure_d1ab1a66f08549e4a848e9eee4d49010@azure.com";
            string pswd = "Ozelot2015";
            SendGridMessage myMessage = new SendGridMessage();
            Console.WriteLine("---------------------- Enviado -----------------------");
            myMessage.AddTo(to);
            myMessage.From = new MailAddress(from, "UT-MATEDI");
            myMessage.Subject = subject;
            myMessage.Text = body;
            //myMessage.AddAttachment(@"C:\Users\Moisés Torres\Documents\Visual Studio 2015\Projects\SmartBaby\SmartBaby\Brigada 09-Abril-2016.xlsx");
            myMessage.AddAttachment(document, name);
            var credentials = new NetworkCredential(username, pswd);

            var transportWeb = new Web(credentials);

            transportWeb.DeliverAsync(myMessage);
        }

        public ActionResult IndexUser()
        {
            var idUser = User.Identity.GetUserId();
            var denunciasUSer = db.Denuncias.Include(d => d.Denunciante).Include(d => d.TipoDenuncia).Where(d =>d.idDenunciante == idUser);
            return View(denunciasUSer.ToList());
        }

        public ActionResult Pdf(int? id)
        {
            Denuncia denuncia = db.Denuncias.Find(id);

            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Expediente/Acuse.rpt")));
            rd.SetParameterValue("folio", denuncia.id);
            rd.SetParameterValue("fecha", denuncia.fecha.ToShortDateString());
            rd.SetParameterValue("hora", denuncia.fecha.ToShortTimeString());
            rd.SetParameterValue("denuncia", denuncia.relatoria);
            rd.SetParameterValue("certificado", denuncia.idDenunciante);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "Application/pdf", "Reporte.pdf");
        }

        // GET: /Denuncias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Denuncia denuncia = db.Denuncias.Find(id);
            if (denuncia == null)
            {
                return HttpNotFound();
            }
            return View(denuncia);
        }

        // GET: /Denuncias/Create
        public ActionResult Create(int id)
        {
            var idUser = User.Identity.GetUserId();
            Denuncia denuncia = new Denuncia();
            denuncia.numExpediente = "000013";
            denuncia.certificado = idUser;
            denuncia.idTipoDenuncia = id;
            denuncia.fecha = DateTime.Now;


            ViewBag.idMinisterioPublico = new SelectList(db.MinisteriosPublicos, "id", "nombre");

   
            return View(denuncia);
        }


        // POST: /Denuncias/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Denuncia denuncia, Responsable responsable, AmpliarDenuncia ad)
        {
            denuncia.idDenunciante = User.Identity.GetUserId();
            denuncia.ipAddress = "192.168.4.210";

            var idUser = User.Identity.GetUserId();
            ApplicationUser usuario = db.Users.Find(idUser);
            
            if (ModelState.IsValid)
            {
                ad.Denuncia = denuncia;
                ad.estatusEvento = EstatusEvento.programado;
                ad.fechaFin = ad.fechaInicio;
                ad.RowVersion = DateTime.Now;
                ad.Denuncia = denuncia;
                responsable.Denuncia = denuncia;
                db.Denuncias.Add(denuncia);
                db.Responsable.Add(responsable);
                db.AmpliacionesDenuncias.Add(ad);
                db.SaveChanges();

                //

                ReportDocument rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Expediente/Acuse.rpt")));
                rd.SetParameterValue("folio", denuncia.id);
                rd.SetParameterValue("fecha", denuncia.fecha.ToShortDateString());
                rd.SetParameterValue("hora", denuncia.fecha.ToShortTimeString());
                rd.SetParameterValue("denuncia", denuncia.relatoria);
                rd.SetParameterValue("certificado", denuncia.idDenunciante);

                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);

                SendAsyncDocument("admin@utancun.edu.mx", usuario.Email, "Acuse", "Hola " + usuario.Email, stream, "Acuse.pdf");

                return RedirectToAction("IndexUser");
            }

            ViewBag.idDenunciante = new SelectList(db.Persona, "id", "nombre", denuncia.idDenunciante);
            ViewBag.idTipoDenuncia = new SelectList(db.TiposDenuncias, "id", "nombre", denuncia.idTipoDenuncia);
			 if (Request.IsAjaxRequest())
                return PartialView(denuncia);
            else
                return View(denuncia);
        }

        // GET: /Denuncias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Denuncia denuncia = db.Denuncias.Find(id);
            if (denuncia == null)
            {
                return HttpNotFound();
            }
            ViewBag.idDenunciante = new SelectList(db.Persona, "id", "nombre", denuncia.idDenunciante);
            ViewBag.idTipoDenuncia = new SelectList(db.TiposDenuncias, "id", "nombre", denuncia.idTipoDenuncia);
 
			 if (Request.IsAjaxRequest())
                return PartialView(denuncia);
            else
                return View(denuncia);
        }

        // POST: /Denuncias/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,numExpediente,idDenunciante,idTipoDenuncia,folioC4,relatoria,fecha,certificado,ipAddress,longitud,latitud,aceptacionPoliticas")] Denuncia denuncia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(denuncia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idDenunciante = new SelectList(db.Persona, "id", "nombre", denuncia.idDenunciante);
            ViewBag.idTipoDenuncia = new SelectList(db.TiposDenuncias, "id", "nombre", denuncia.idTipoDenuncia);
            if (Request.IsAjaxRequest())
                return PartialView(denuncia);
            else
                return View(denuncia);
        }

        // GET: /Denuncias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Denuncia denuncia = db.Denuncias.Find(id);
            if (denuncia == null)
            {
                return HttpNotFound();
            }
            return View(denuncia);
        }

        // POST: /Denuncias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Denuncia denuncia = db.Denuncias.Find(id);
            db.Denuncias.Remove(denuncia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult List()
        {
            var tipoDenuncias = db.TiposDenuncias;
            return View(tipoDenuncias.ToList());
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
