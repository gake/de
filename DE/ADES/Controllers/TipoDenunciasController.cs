﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ADES.Models;
using IdentitySample.Models;

namespace ADES.Controllers
{
    public class TipoDenunciasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /TipoDenuncias/
        public ActionResult Index()
        {
            return View(db.TiposDenuncias.ToList());
        }

        // GET: /TipoDenuncias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoDenuncia tipoDenuncia = db.TiposDenuncias.Find(id);
            if (tipoDenuncia == null)
            {
                return HttpNotFound();
            }
            return View(tipoDenuncia);
        }

        // GET: /TipoDenuncias/Create
        public ActionResult Create()
        {
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }


        // POST: /TipoDenuncias/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,nombre,descipcion,IsDeleted,RowVersion")] TipoDenuncia tipoDenuncia)
        {
            if (ModelState.IsValid)
            {
                db.TiposDenuncias.Add(tipoDenuncia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

			 if (Request.IsAjaxRequest())
                return PartialView(tipoDenuncia);
            else
                return View(tipoDenuncia);
        }

        // GET: /TipoDenuncias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoDenuncia tipoDenuncia = db.TiposDenuncias.Find(id);
            if (tipoDenuncia == null)
            {
                return HttpNotFound();
            }
 
			 if (Request.IsAjaxRequest())
                return PartialView(tipoDenuncia);
            else
                return View(tipoDenuncia);
        }

        // POST: /TipoDenuncias/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,nombre,descipcion,IsDeleted,RowVersion")] TipoDenuncia tipoDenuncia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoDenuncia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
                return PartialView(tipoDenuncia);
            else
                return View(tipoDenuncia);
        }

        // GET: /TipoDenuncias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoDenuncia tipoDenuncia = db.TiposDenuncias.Find(id);
            if (tipoDenuncia == null)
            {
                return HttpNotFound();
            }
            return View(tipoDenuncia);
        }

        // POST: /TipoDenuncias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoDenuncia tipoDenuncia = db.TiposDenuncias.Find(id);
            db.TiposDenuncias.Remove(tipoDenuncia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
