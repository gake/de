﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ADES.Models;
using IdentitySample.Models;

namespace ADES.Controllers
{
    public class PersonalesMPController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /PersonalesMP/
        public ActionResult Index()
        {
            var persona = db.Persona.OfType<PersonalMP>().Include(p => p.ApplicationUser);
            return View(persona.ToList());
        }

        // GET: /PersonalesMP/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonalMP personalMP = (PersonalMP)db.Persona.Find(id);
            if (personalMP == null)
            {
                return HttpNotFound();
            }
            return View(personalMP);
        }

        // GET: /PersonalesMP/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Users, "Id", "Email");
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }


        // POST: /PersonalesMP/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,nombre,apellidoPaterno,apellidoMaterno,numIdentificación,curp,fechaNacimiento,telefono,celular,tipoIdentificacion,sexo,estadoCivil,fechaAlta,numEmplead,mesa")] PersonalMP personalMP)
        {
            if (ModelState.IsValid)
            {
                db.Persona.Add(personalMP);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id = new SelectList(db.Users, "Id", "Email", personalMP.id);
			 if (Request.IsAjaxRequest())
                return PartialView(personalMP);
            else
                return View(personalMP);
        }

        // GET: /PersonalesMP/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonalMP personalMP = (PersonalMP)db.Persona.Find(id);
            if (personalMP == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Users, "Id", "Email", personalMP.id);
 
			 if (Request.IsAjaxRequest())
                return PartialView(personalMP);
            else
                return View(personalMP);
        }

        // POST: /PersonalesMP/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,nombre,apellidoPaterno,apellidoMaterno,numIdentificación,curp,fechaNacimiento,telefono,celular,tipoIdentificacion,sexo,estadoCivil,fechaAlta,numEmplead,mesa")] PersonalMP personalMP)
        {
            if (ModelState.IsValid)
            {
                db.Entry(personalMP).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id = new SelectList(db.Users, "Id", "Email", personalMP.id);
            if (Request.IsAjaxRequest())
                return PartialView(personalMP);
            else
                return View(personalMP);
        }

        // GET: /PersonalesMP/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonalMP personalMP = (PersonalMP)db.Persona.Find(id);
            if (personalMP == null)
            {
                return HttpNotFound();
            }
            return View(personalMP);
        }

        // POST: /PersonalesMP/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PersonalMP personalMP = (PersonalMP)db.Persona.Find(id);
            db.Persona.Remove(personalMP);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
